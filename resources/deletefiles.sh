#!/bin/bash

# number of days
num_days=$DELETE_FILES_OLDER_THAN

# S3 bucket name
bucket_name=$AWS_BUCKET_NAME
folder_path=$AWS_BUCKET_BACKUP_PATH

# Log the start of the script
echo "Starting script to delete files older than $num_days days from $folder_path..."

# Get list of all files in folder
echo "Getting list of all files in folder..."
files=$(aws s3 ls s3://$bucket_name/$folder_path --recursive | awk '{print $4}')

# Loop through files and delete those older than num_days
for file in $files
do
  # Check if the file is a directory and skip if it is
  if [[ $file == */ ]]; then
    echo "$file is a directory. Skipping deletion."
    continue
  fi
  # Get last modified date of file
  last_modified=$(aws s3api head-object --bucket $bucket_name --key $file --query LastModified --output text)

  # Convert last modified date to seconds since epoch
  last_modified_secs=$(date -d "$last_modified" +%s)

  # Get current date in seconds since epoch
  current_date_secs=$(date +%s)

  # Calculate the difference in seconds between current date and last modified date
  diff_secs=$((current_date_secs - last_modified_secs))

  # Convert num_days to seconds
  num_days_secs=$((num_days * 86400))

  # Check if file is older than num_days
  if [ $diff_secs -gt $num_days_secs ]; then
    # Log the deletion of the file
    echo "Deleting $file..."
    aws s3 rm s3://$bucket_name/$file
  else
    # Log that the file is not being deleted
    echo "File $file is not older than $num_days days. Skipping deletion.."
  fi
done

# Log the end of the script
echo "Files deleted successfully from $folder_path. See log file for details."