#/bin/sh 


# Set the flag to false
has_failed=false


# Loop through all the folder names for the current Jenkins instance
for folder_name in $(curl -sSL --user $TARGET_JENKINS_USER:$TARGET_JENKINS_PASSWORD --silent "$TARGET_JENKINS_HOST:$TARGET_JENKINS_PORT/api/json" | jq -r '.jobs[] | select(._class == "com.cloudbees.hudson.plugins.folder.Folder").name')
do
    # Check if the folder exists in the backup path. If it doesn't exist, create it.
    if ! aws s3 ls "$AWS_BUCKET_NAME/$AWS_BUCKET_BACKUP_PATH/${folder_name}/" &> /dev/null
    then
        aws s3api put-object --bucket "$AWS_BUCKET_NAME" --key "$AWS_BUCKET_BACKUP_PATH/${folder_name}/" --content-length 0 &> /dev/null
        echo -e "S3 folder created for ${folder_name}"
    fi

    # Loop through all the job names for the current folder
    for job_name in $(curl --user $TARGET_JENKINS_USER:$TARGET_JENKINS_PASSWORD --silent -sSL "$TARGET_JENKINS_HOST:$TARGET_JENKINS_PORT/job/${folder_name}/api/json" | jq -r '.jobs[].name')
    do
        # Check if the folder for the current job exists in the backup path. If it doesn't exist, create it.
        if ! aws s3 ls "$AWS_BUCKET_NAME/$AWS_BUCKET_BACKUP_PATH/${folder_name}/${job_name}/" &> /dev/null
        then
            aws s3api put-object --bucket "$AWS_BUCKET_NAME" --key "$AWS_BUCKET_BACKUP_PATH/${folder_name}/${job_name}/" --content-length 0 &> /dev/null
            echo -e "S3 folder created for ${job_name} in ${folder_name}"
        fi

        # Perform the Jenkins backup. Put the output to a variable. If successful upload the backup to S3, if unsuccessful print an entry to the console and the log, and set has_failed to true.
        if jenkinsoutput=$(curl --user $TARGET_JENKINS_USER:$TARGET_JENKINS_PASSWORD --silent "$TARGET_JENKINS_HOST:$TARGET_JENKINS_PORT/job/${folder_name}/job/${job_name}/config.xml" > "/tmp/${job_name}-config-$(date +'%d-%m-%Y-%H').xml")
        then
            echo -e "Jenkins backup successfully completed for ${job_name} in ${folder_name} at $(date +'%d-%m-%Y %H:%M:%S')."

            # Perform the upload to S3. Put the output to a variable. If successful, print an entry to the console and the log. If unsuccessful, set has_failed to true and print an entry to the console and the log
            if awsoutput=$(aws s3 cp "/tmp/${job_name}-config-$(date +'%d-%m-%Y-%H').xml" s3://$AWS_BUCKET_NAME/$AWS_BUCKET_BACKUP_PATH/${folder_name}/${job_name}/config-$(date +'%d-%m-%Y-%H').xml 2>&1)
            then
                echo -e "Jenkins backup successfully uploaded for ${job_name} in ${folder_name} at $(date +'%d-%m-%Y %H:%M:%S')."
            else
                echo -e "Jenkins backup failed to upload for ${job_name} in ${folder_name} at $(date +'%d-%m-%Y %H:%M:%S'). Error: ${awsoutput}" | tee -a /tmp/kubernetes-s3-jenkins-backup.log
                has_failed=true
            fi
        else
            echo -e "Jenkins backup FAILED for $job_name at $(date +'%d-%m-%Y %H:%M:%S'). Error: $jenkinsoutput" | tee -a /tmp/kubernetes-s3-jenkins-backup.log
            has_failed=true
        fi
    done
done


# Call delete_s3_files.sh script to delete files.
bash /deletefiles.sh

# Check if any of the backups have failed. If so, exit with a status of 1. Otherwise exit cleanly with a status of 0.
if [ "$has_failed" = true ]
then

    # If Slack alerts are enabled, send a notification alongside a log of what failed
    if [ "$SLACK_ENABLED" = true ]
    then
        # Put the contents of the JENKINS backup logs into a variable
        logcontents=`cat /tmp/kubernetes-s3-jenkins-backup.log`

        # Send Slack alert
        /slack-alert.sh "One or more backups on JENKINS host $TARGET_JENKINS_HOST failed. The error details are included below:" "$logcontents"
    fi

    echo -e "kubernetes-s3-jenkins-backup encountered 1 or more errors. Exiting with status code 1."
    exit 1

else

    # If Slack alerts are enabled, send a notification that all JENKINS backups were successful
    if [ "$SLACK_ENABLED" = true ]
    then
        /slack-alert.sh "All Jenkins Jobs backups successfully completed from $ENVIRONMENT Jenkins and stored in  s3://$AWS_BUCKET_NAME/$AWS_BUCKET_BACKUP_PATH and deleted files older than $DELETE_FILES_OLDER_THAN days ."
    fi

    exit 0
    
fi
